[![pipeline status](https://gitlab.com/bengtlueers.de/bengtlueers.de/badges/master/pipeline.svg)](https://gitlab.com/bengtlueers.de/bengtlueers.de/-/commits/master)
[![coverage report](https://gitlab.com/bengtlueers.de/bengtlueers.de/badges/master/coverage.svg)](https://gitlab.com/bengtlueers.de/bengtlueers.de/-/commits/master)

My personal homepage.

## URLs

This should be available under the following URLS:

-   <https://xn--bengtlers-v9a.gitlab.io>
-   <https://www.xn--bengtlers-v9a.de>
-   <https://www.bengtlüers.de>
-   <https://xn--bengtlers-v9a.de>
-   <https://bengtlüers.de>
 